﻿using System;
using System.Collections.Generic;
using System.Data;


namespace Empiria.Data.Modeling {
  public class DBDependencies {

    #region Constructors and parsers

    private DBDependencies(string databaseName, string tableName) {
      this.TableName = tableName;
      this.DatabaseName = databaseName;
      this.DependentObject = string.Empty;   
      this.Description = string.Empty;
    }

    public static DBDependencies Parse(string databaseName, string tableName){
      var dbDependcies = new DBDependencies(databaseName, tableName);
      return dbDependcies;
    }

    #endregion Constructors and parsers

    #region Properties

    public string DatabaseName {
      get;
      private set;
    }

    public string TableName {
      get;
      private set;
    }

    public string DependentObject {
      get;
      internal set;
    }
        
    public string Description {
      get;
      internal set;
    }

    #endregion Properties

  }
}
