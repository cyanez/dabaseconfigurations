﻿using System;
using System.Collections.Generic;
using System.Data;

namespace SqlConfigObjects {
  class DBParameter {

  #region Constructors and parsers
    
    public DBParameter(string queryName){
      this.QueryName = queryName;
      this.Index = 0;
      this.Name = string.Empty;
      this.SqlDbType = 0;
      this.Direction = 0;
      this.Size = 0;
      this.Scale = 0;
      this.Precision = 0;
      this.DefaultValue = string.Empty;
    }   
    
    #endregion Constructors and parsers

  #region Public properties

    public string QueryName{
      get;
      private set;
    }

    public int Index{
      get;
      private set;

    }

    public string Name{
      get;
      private set;
    }

    public int SqlDbType{
      get;
      private set;
    }

    public int Direction{
      get;
      private set;
    }

    public int Size{
      get;
      private set;
    }

    public int Scale{
      get;
      private set;
    }

    public int Precision{
      get;
      private set;
    }

    public string DefaultValue{
      get;
      private set;
    }       

		#endregion Public properties

  #region Public methods
        
    public List<DBParameter> GetQueryParameters(){
      
      List<DBParameter> queryParameters = new List<DBParameter>();
      DataServices dataServices = new DataServices();
      DataTable queryParametersTable = dataServices.GetQueryParameters(this.QueryName);
      
      foreach(DataRow row in queryParametersTable.Rows){
        DBParameter queryParameter = new DBParameter(this.QueryName);
        queryParameter.Index = (int)row["Index"];
        queryParameter.Name = row["Name"].ToString();
        queryParameter.SqlDbType = Convert.ToInt32(row["Type"]);
        queryParameter.Direction = Convert.ToInt32(row["Direction"]);
        queryParameter.Size = Convert.ToInt32(row["Size"]);
        queryParameter.Scale = Convert.ToInt32(row["Scale"]);
        queryParameter.Precision = Convert.ToInt32(row["Precision"]);
        queryParameter.DefaultValue = string.Empty;
        queryParameters.Add(queryParameter);
      }

      return queryParameters;
    }           
    

    #endregion Public methods
  }
}
