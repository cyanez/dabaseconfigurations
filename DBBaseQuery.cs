﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Empiria.Data.Modeling {
 public abstract class DBBaseQuery {

    #region Fields

    DBDataSource dbDataSource;

    #endregion Fields

    #region Constructors and parsers

    protected DBBaseQuery(string dataBaseName) {
      this.DataBaseName = dataBaseName;
      this.QueryName = string.Empty;
      this.CreateDate = DateTime.MinValue;
      this.ModifyDate = DateTime.MinValue;
      dbDataSource = DBDataSource.Parse(this.DataBaseName);     
    }

    #endregion Constructors and parsers

    #region Properties

    public string DataBaseName {
      get;
      protected set;
    }

    public string QueryName {
      get;
      internal set;
    }

    public string SqlCode {
      get { return GetSqlCode(); }
    }

    public int Count {
      get { return GetParametersCount(); }
    }

    public DateTime CreateDate {
      get;
      internal set;
    }

    public DateTime ModifyDate {
      get;
      internal set;
    }

    public List<DBQueryParameter> Paremeters {
      get { return dbDataSource.GetQueryParameters(this.QueryName);}      
    }

    #endregion Properties

    #region Private methods

    private string GetSqlCode() {
      return dbDataSource.GetObjectCode(this.QueryName);
    }

    private int GetParametersCount() {
      List<DBQueryParameter> queryParametersList = dbDataSource.GetQueryParameters(this.QueryName);
      return queryParametersList.Count;
    }

    


    #endregion Private methods

  }
}
