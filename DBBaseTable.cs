﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Empiria.Data.Modeling {
  public abstract class DBBaseTable {

    public enum DbTableType {
      Undefined,
      dbTable,
      dbView
    }

    #region Fields

    private DBDataSource dbDataSource;

    #endregion Fields

    #region Constructors and parsers

    protected DBBaseTable(string dataBaseName) {
      this.DatabaseName = dataBaseName;
      this.TableName = string.Empty;
      this.CreateDate = DateTime.MinValue;
      this.ModifyDate = DateTime.MinValue;
      this.TableType = DbTableType.Undefined;
      dbDataSource = DBDataSource.Parse(dataBaseName);
    }

    #endregion Constructors and parsers

    #region Public properties

    public string DatabaseName {
      get;
      private set;
    }

    public string TableName {
      get;
      internal set;
    }

    public int CountColumns {
      get { return GetCountColumns(); }
    }

    public DateTime CreateDate {
      get;
      internal set;
    }

    public DateTime ModifyDate {
      get;
      internal set;
    }

    public DbTableType TableType {
      get;
      set;
    }

    public List<DBField> Fields {
      get { return GetFields(); }
    }

    public List<DBDependencies> Dependencies {
      get { return GetTableDependencies(); }
    }

    #endregion Public properties

    #region Protected methods

    protected abstract int GetCountColumns();

    #endregion

    #region Private methods


    private List<DBField> GetFields() {
      return dbDataSource.GetFields(this.TableName, this.TableType);
    }        

    private List<DBDependencies> GetTableDependencies(){
      return dbDataSource.GetTableDependencies(this.TableName);
    }

    #endregion
  }
}
