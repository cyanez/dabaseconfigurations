﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace SqlConfigObjects {
  class DBProcedures {

  #region Fields

    DataServices dataService = new DataServices();

    #endregion Fields

  #region Constructors and parsers
    
    public DBProcedures(string dataBaseName){
      this.DataBaseName = dataBaseName;
      this.Name = string.Empty;      
      this.CreateDate = DateTime.MinValue;
      this.ModifyDate = DateTime.MinValue;
      this.FunctionType = string.Empty;
    }        

    #endregion Constructors and parsers

  #region Properties

    public string DataBaseName {
      get;
      private set;
    }

    public string Name{
      get;
      private set;
    }

    public string Code {
      get {return GetCode();}
    }

    public int ParametersCount {
      get { return GetParametersCount(); }
    }

    public DateTime CreateDate{
      get;
      private set;
    }

    public DateTime ModifyDate{
      get;
      private set;      
    }           
        
    public string FunctionType{
      get;
      private set;
    }       
    
    #endregion Properties

  #region Private methods

    private string GetCode() {     
        return dataService.GetObjectCode(this.DataBaseName, this.Name);      
    }
    private int GetParametersCount() {

      DBParameter queryParameters = new DBParameter(this.Name);
      List<DBParameter> queryParametersList = queryParameters.GetQueryParameters();
      return queryParametersList.Count;

    }

  #endregion Private methods

  #region Public methods
    public List<DBProcedures> GetProcedures(){

      List<DBProcedures> procedures = new List<DBProcedures>();
      DataTable proceduresTable = dataService.GetStoredProcuduresAndFunctions(this.DataBaseName);
      foreach (DataRow row in proceduresTable.Rows) {
        DBProcedures procedure = new DBProcedures(this.DataBaseName);
        procedure.Name = row["Name"].ToString();       
        procedure.CreateDate = (DateTime) row["CreateDate"];
        procedure.ModifyDate = (DateTime) row["ModifyDate"];
        procedure.FunctionType = row["Type"].ToString();
        procedures.Add(procedure);
      }
      return procedures;    
    }
   

    public List<DBProcedures> GetProcedures(string[] exceptions){
      
      int index = 0;
      List<DBProcedures> procedures = GetProcedures();
      foreach(string exception in exceptions){
        index = procedures.FindIndex(procedure => procedure.Name == exception);
        if (index != -1) {
          procedures.RemoveAt(index);
        } else {
          //no-op
        }
      }

      return procedures;
    }
  
    #endregion Public methods

  }
}
