﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Empiria.Data.Modeling {
  public class DBDatabase {
  
  private DBDataSource  dbDataSource;

    #region Constructors and parsers
        
    private DBDatabase(string databaseName){
      this.DatabaseName = databaseName;
      dbDataSource = DBDataSource.Parse(this.DatabaseName);
    }    

    public static DBDatabase Parse(string dataBaseName){
      var dbDatabase = new DBDatabase(dataBaseName);
      return dbDatabase;
    }           
     
    #endregion Constructors and parsers

    #region Public properties

    public string DatabaseName{
      get;
      private set;
    }

    public List<DBTable> Tables{
      get {return dbDataSource.GetTables();}
    }

    public List<DBView> Views{
      get{return dbDataSource.GetViews();}
    }
    
    public List<DBStoredProcedure> StoredProcedures{
      get{return dbDataSource.GetStoredProcedures(); }
    }   

    public List<DBFunction> Functions{
      get{ return dbDataSource.GetFunctions(); }
    }

    #endregion Public properties

    public List<DBTable> GetTables(string[] exceptions){
      return dbDataSource.GetTables(exceptions);
    }

    public List<DBView> GetViews(string[] exceptions){
      return dbDataSource.GetViews(exceptions);
    }

    public List<DBStoredProcedure> GetStoredProcedures(string[] exceptions) {
      return dbDataSource.GetStoredProcedures(exceptions);
    }
              
  }
}
