﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace SqlConfigObjects {
  class DataServices {


  private string connectionString = "Server=CHRIS-PC\\SQLEXPRESS;Database=Zacatecas;Trusted_Connection=True;";

  #region General Methods

  private DataTable GetObjectTable(string sql) {    
    SqlConnection conn = new SqlConnection(connectionString);
    SqlDataAdapter adapter = new SqlDataAdapter(sql, conn);
    DataTable tables = new DataTable();
    adapter.Fill(tables);
    return tables;
  }

  private int GetIntValue(string sql) {
    DataTable table = GetObjectTable(sql);
    if (table.Rows.Count == 0) {
      return 0;
    } else {
      return Convert.ToInt32(table.Rows[0][0]);
    }
  }

  private string GetStringValue(string sql) {
    DataTable table = GetObjectTable(sql);
    if (table.Rows.Count == 0) {
      return string.Empty;
    } else {
      return table.Rows[0][0].ToString();
    }
  }

  #endregion General Methods

  #region Tables and Views

  public DataTable GetDataBaseTables(string dataBaseName) {
      string sql = "USE " + dataBaseName + " " +
                   "SELECT * FROM sys.tables o " +
                   "ORDER BY o.name ";
      return GetObjectTable(sql);
  }

  public int GetTableRows(string dataBaseName, string tableName) {
    string sql = "USE " + dataBaseName + " " +
                 "SELECT CONVERT(bigint, rows) TotalRows " +
                 "FROM sysindexes  " +
                 "WHERE id = OBJECT_ID(" + " '" + tableName + "' " + ") " +
                 "AND indid < 2 ";
    return GetIntValue(sql);
  }

  public int GetTableColumns(string dataBaseName, string tableName) {
    string sql = "USE " + dataBaseName + " " +
                 "SELECT COUNT(c.column_id) AS TotalColumn " +
                 "FROM sys.tables t INNER JOIN sys.columns c " +
                 "ON t.object_id = c.object_id AND t.name = " + "'" + tableName + "'";
    return GetIntValue(sql);
  }
    
  public DataTable GetViews(string dataBaseName) {
    string sql = "USE " + dataBaseName + " " +
                 "SELECT * FROM sys.views o " +
                 "ORDER BY o.name ";
    return GetObjectTable(sql);
  }

  public int GetViewColumns(string dataBaseName, string viewName) {
    string sql = "USE " + dataBaseName + " " +
                 "SELECT COUNT(c.column_id) AS TotalColumn " +
                 "FROM sys.views t INNER JOIN sys.columns c " +
                 "ON t.object_id = c.object_id AND t.name = " + "'" + viewName + "'";
    return GetIntValue(sql);
  }

  #endregion Tables and Views

  #region Get object code

  public string GetObjectCode(string dataBaseName, string objectName) {
    string sql = "USE " + dataBaseName + " " +
                 "SELECT DEFINITION AS Code " +
                 "FROM sys.sql_modules " +
                 "WHERE object_id = OBJECT_ID( " + "'" + objectName + "'" + " )";
    return GetStringValue(sql);
  }

  #endregion Get object code

  #region Fields

  public DataTable GetTableFields(string dataBaseName, string tableName) {
    string sql = "USE " + dataBaseName + "  " +
                 "SELECT  c.column_id AS FieldId, c.name AS FieldName,  c.user_type_id AS DataType, " +
                 "c.max_length AS MaxLength, c.precision AS Precision, c.scale AS Scale, " +
                 "c.is_identity AS IsIdentity, c.is_computed AS IsComputed, c.is_nullable AS IsNullable, " +
                 "c.is_replicated AS IsReplicated,  object_definition(default_object_id) AS DefaultValue, " +
                 "c.collation_name AS Collation " +
                 "FROM sys.columns c, sys.tables v " +
                 "WHERE (c.object_id = v.object_id) " +
                 "AND (v.name = " + "'" + tableName + "')";
    return GetObjectTable(sql);
  }

  public DataTable GetViewFields(string dataBaseName, string viewName) {
    string sql = "USE " + dataBaseName + "  " +
                 "SELECT  c.column_id AS FieldId, c.name AS FieldName,  c.user_type_id AS DataType, " +
                 "c.max_length AS MaxLength, c.precision AS Precision, c.scale AS Scale, " +
                 "c.is_identity AS IsIdentity, c.is_computed AS IsComputed, c.is_nullable AS IsNullable, " +
                 "c.is_replicated AS IsReplicated,  object_definition(default_object_id) AS DefaultValue, " +
                 "c.collation_name AS Collation " +
                 "FROM sys.columns c, sys.views v " +
                 "WHERE (c.object_id = v.object_id) " +
                 "AND (v.name = " + "'" + viewName + "')";
    return GetObjectTable(sql);
  }

  public DataTable GetPrimaryKeys(string tableName) {
    string sql = "exec [sys].[sp_primary_keys_rowset] @table_name= '" + tableName + "'";
    return GetObjectTable(sql);
  }


  #endregion Fields

  #region Functions and StoredProcedures  

  public DataTable GetStoredProcedures(string dataBaseName) {
    string sql = "USE " + dataBaseName + " " +
                 "SELECT a.Name AS Name, a.type_desc AS Type, a.create_date AS CreateDate, a.modify_date AS ModifyDate " +
                 "FROM sys.objects a " +
                 "INNER JOIN sys.schemas b " +
                 "ON a.schema_id = b.schema_id " +
                 "WHERE (TYPE = 'P') " +
                 "ORDER BY Type, Name ";
    return GetObjectTable(sql);
  }

  public DataTable GetFunctions(string dataBaseName) {
    string sql = "USE " + dataBaseName + " " +
                 "SELECT a.Name AS Name, a.type_desc AS Type, a.create_date AS CreateDate, a.modify_date AS ModifyDate " +
                 "FROM sys.objects a " +
                 "INNER JOIN sys.schemas b " +
                 "ON a.schema_id = b.schema_id " +
                 "WHERE TYPE in ('FN', 'IF', 'TF') " +
                 "ORDER BY Type, Name ";
    return GetObjectTable(sql);
  }
  #endregion

  #region Parameter 

  protected DataTable dtParamTable() {
    DataTable dtPTable = new DataTable();
    DataColumn dcIndex = new DataColumn("Index", typeof(int));
    DataColumn dcName = new DataColumn("Name", typeof(string));
    DataColumn dcType = new DataColumn("Type", typeof(string));
    DataColumn dcPrecision = new DataColumn("Precision", typeof(string));
    DataColumn dcDecimal = new DataColumn("Scale", typeof(string));
    DataColumn dcSize = new DataColumn("Size", typeof(string));
    DataColumn dcDirection = new DataColumn("Direction", typeof(string));
    dtPTable.Columns.Add(dcIndex);
    dtPTable.Columns.Add(dcName);
    dtPTable.Columns.Add(dcType);
    dtPTable.Columns.Add(dcPrecision);
    dtPTable.Columns.Add(dcDecimal);
    dtPTable.Columns.Add(dcSize);
    dtPTable.Columns.Add(dcDirection);
    return dtPTable;
  }

  public DataTable GetQueryParameters(string queryName) {
    SqlConnection scConnect = new SqlConnection(connectionString);
    SqlCommand scParams = new SqlCommand();
    scParams.Connection = scConnect;
    scParams.CommandText = queryName;
    scParams.CommandType = CommandType.StoredProcedure;
    scParams.Connection.Open();
    SqlCommandBuilder.DeriveParameters(scParams);
    scParams.Connection.Close();
    int intCountParameters = scParams.Parameters.Count;
    DataTable dtParamsTable = dtParamTable();
    for (int i = 0; i < intCountParameters; i++) {
      if (scParams.Parameters[i].Direction == ParameterDirection.Input) {
        DataRow Row = dtParamsTable.NewRow();
        Row["Index"] = i;
        Row["Name"] = scParams.Parameters[i].ParameterName.ToString();
        Row["Type"] = (int) scParams.Parameters[i].SqlDbType;
        Row["Precision"] = scParams.Parameters[i].Precision.ToString();
        Row["Scale"] = scParams.Parameters[i].Scale.ToString();
        Row["Size"] = scParams.Parameters[i].Size.ToString();
        Row["Direction"] = (int) scParams.Parameters[i].Direction;
        dtParamsTable.Rows.Add(Row);
      }
    }
    return dtParamsTable;
  }

  #endregion

  #region index

  public bool HaveTableIndex(string dataBaseName, string tableName) {
    DataTable indexTable = GetTableIndex(dataBaseName, tableName);
    if (indexTable.Rows.Count == 0) {
      return false;
    } else {
      return true;
    }
  }

  public DataTable GetTableIndex(string dataBaseName, string tableName){
    string sql = "USE " + dataBaseName + "  " +
                 "SELECT so.name AS TableName , si.name AS IndexName , si.type_desc AS IndexType " +			
                 "FROM sys.indexes si " +
                 "JOIN sys.objects so ON si.[object_id] = so.[object_id] " +
                 "WHERE so.type = 'U' " +   
                 "AND (si.name IS NOT NULL) " +
			           "AND so.name = " +"'" + tableName + "' " +
                 "ORDER BY IndexType ";
    return GetObjectTable(sql);
  }  
 
  #endregion

  #region Dependencies

  public DataTable GetTableDependences(string dataBaseName, string TableName) {
    string sql = "USE " + dataBaseName + " " +
                 "SELECT  referencing_entity_name AS DependenceName, " +
                 "referencing_class_desc AS EntityDescription " +
                 "FROM sys.dm_sql_referencing_entities ('dbo." + TableName + "', 'OBJECT'); ";
    return GetObjectTable(sql);
  }
  /// <summary>
  /// Get the tables or views who composed a Stored Procedure or View
  /// </summary>
  /// <param name="dataBaseName"></param>
  /// <param name="objectName">Function, Stored Procedure or View</param>
  /// <returns></returns>
  public DataTable GetComposedBy(string dataBaseName, string objectName) {
    string sql = "USE " + dataBaseName + " " +
                  "SELECT  o.name AS ReferencingObject, " +
                  "SED.referenced_entity_name AS ReferencedObject " +
                  "FROM sys.all_objects O INNER JOIN sys.sql_expression_dependencies SED " +
                  "ON O.OBJECT_ID=SED.REFERENCING_ID  " +
                  "WHERE O.name = " + "'" + objectName + "' " +
                  "ORDER BY ReferencedObject ";
    return GetObjectTable(sql);
  }

  public DataTable GetObjecstWithDependences(string dataBaseName, string columnName){
    string sql = "USE " + dataBaseName + " " +
                 "SELECT obj.Name SPName, sc.TEXT SPText " +
                 "FROM sys.syscomments sc " +
                 "INNER JOIN sys.objects obj ON sc.Id = obj.OBJECT_ID " +
                 "WHERE sc.TEXT LIKE '%' + '" + columnName + "' + '%' " +
                 "AND  TYPE IN ('V','FN', 'IF', 'TF','P') ";
   return GetObjectTable(sql);
  }

  public DataTable GetObjectsWithColumn(string dataBaseName, string columnName){
    string sql = "USE " + dataBaseName + " " +
                 "SELECT obj.Name SPName, sc.TEXT SPText " +
                 "FROM sys.syscomments sc " +
                 "INNER JOIN sys.objects obj ON sc.Id = obj.OBJECT_ID " +
                 "WHERE sc.TEXT LIKE '%' + '" + columnName + "' + '%' " +
                 "AND  TYPE IN ('V','FN', 'IF', 'TF','P') ";
    return GetObjectTable(sql);
  }

  
  

  #endregion

  }
}
