﻿using System;
using System.Collections.Generic;

namespace Empiria.Data.Modeling {
  public class DBFunction :DBBaseQuery {

    #region Constructors and parsers

    private DBFunction(string databaseName): base(databaseName) {
      this.Type = string.Empty;
    }

    public static DBFunction Parse(string databaseName){
      var dbFuction = new DBFunction(databaseName);
      return dbFuction;
    }

    #endregion Constructors and parsers

    #region Properties

    public string Type {
      get;
      internal set;
    }

    #endregion Properties

    

  }
}
