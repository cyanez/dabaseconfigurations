﻿using System;
using System.Collections.Generic;


namespace Empiria.Data.Modeling {
  public class DBStoredProcedure : DBBaseQuery {
      
    #region Constructors and parsers

    private DBStoredProcedure(string databaseName): base(databaseName) {
    }

    public static DBStoredProcedure Parse(string databaseName){
      var dbSp = new DBStoredProcedure(databaseName);
      return dbSp;
    }

    #endregion Constructors and parsers

    

    


  }
}
