﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Empiria.Data.Modeling {
  class Program {
    static void Main(string[] args) {
      var db = DBDatabase.Parse("Zacatecas");
      List<DBTable> dbtTableList = db.Tables;
      foreach (DBTable table in dbtTableList) {
        PrintTable(table);
        PrintFields(table.Fields);
        PrintIndexes(table.Index);
        PrintDependeces(table.Dependencies);
      }    

      //List<DBStoredProcedure> procedures = db.StoredProcedures;
      //foreach (DBStoredProcedure procedure in procedures) {
      //  PrintStoredProcedrues(procedure);
      //  PrintQueryParameters(procedure.Paremeters);
      //}   
      
      //List<DBFunction> functions = db.Functions;
      //foreach (DBFunction function in functions) {
      //  PrintFunctions(function);
      //  PrintQueryParameters(function.Paremeters);
      //}
      //Console.WriteLine(db.GetTable());
      Console.ReadKey();
    }

    static void PrintTable(DBTable table) {
      Console.WriteLine("Nombre: " + table.TableName);
      Console.WriteLine("Columnas:" + table.CountColumns.ToString());
      Console.WriteLine("Renglones:" + table.CountRows);
      Console.WriteLine("Tipo: " + table.TableType.ToString());
      Console.WriteLine();
    }

    static void PrintView(DBView view){
      Console.WriteLine("Nombre: " + view.TableName);
      Console.WriteLine("Columnas:" + view.CountColumns.ToString());
      Console.WriteLine("Renglones:" + view.SqlCode);
      Console.WriteLine();
    }

    static void PrintFields(List<DBField> fields) {
      foreach (DBField field in fields) {
        Console.WriteLine("Nombre Campo: " + field.Name);
        Console.WriteLine("# de Campo: " + field.Index);
        Console.WriteLine("Tipo: " + field.DataType);        
        Console.WriteLine("Longitud: " + field.MaxLength);
        Console.WriteLine("Primary Key: " + field.IsPrimaryKey);
      }
      Console.WriteLine();
    }

    static void PrintIndexes(List<DBIndex> indexes) {
      foreach (DBIndex index in indexes) {
        Console.WriteLine("Nombre Indice: " + index.Name);
        Console.WriteLine("Typo: " + index.Type);
      }
      Console.WriteLine();
    }

    static void PrintDependeces(List<DBDependencies> dependencies) {
      foreach (DBDependencies dependence in dependencies) {
        Console.WriteLine("Objeto Dependendiente: " + dependence.DependentObject);
        Console.WriteLine("Descripción: " + dependence.Description);
      }
      Console.WriteLine();
    }

    static void PrintStoredProcedrues(DBStoredProcedure procedure){
      Console.WriteLine("Nombre: " + procedure.QueryName);
      Console.WriteLine("Fecha Creado: " +  procedure.CreateDate);
      Console.WriteLine("Cantidad de Parametros: " + procedure.Count);
      Console.WriteLine("Código: "  + procedure.SqlCode); 
      Console.WriteLine();
    }

    static void PrintFunctions(DBFunction function) {
      Console.WriteLine("Nombre: " + function.QueryName);
      Console.WriteLine("Tipo: " + function.Type);
      Console.WriteLine("Fecha Creada: " + function.CreateDate);
      Console.WriteLine("Cantidad de Parametros: " + function.Count);
      Console.WriteLine("Código: " + function.SqlCode);
      Console.WriteLine();
    }
    static void PrintQueryParameters(List<DBQueryParameter> parameters){
      foreach(DBQueryParameter parameter in parameters){
        Console.WriteLine("Nombre: " + parameter.Name);
        Console.WriteLine("Parametro Index: " + parameter.Index);
        Console.WriteLine("Tipo: " + parameter.SqlDbType);
        Console.WriteLine("Direction: " + parameter.Direction);
        Console.WriteLine("Tamaño: " + parameter.Size);
        Console.WriteLine("Precisión: " + parameter.Precision);
      }
    }

  }
}
